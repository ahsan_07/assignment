<?php

use Illuminate\Database\Seeder;

class QuestionareTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('questionare')->insert([
        	[
        	'id'	        => 1,
            'name'          => 'Mathematics',
            'duration'      => 3,
            'unit'          => 'hr',
            'is_resumable'  => 0,
            'is_published'  => 1,
            'publisher_id'  => 1
        ],
        [
        	'id'	        => 2,
            'name'          => 'Science',
            'duration'      => 30,
            'unit'          => 'min',
            'is_resumable'  => 1,
            'is_published'  => 1,
            'publisher_id'  => 1
        ]

        ]);
    }
}
