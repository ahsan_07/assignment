<?php

use Illuminate\Database\Seeder;

class AnswerTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('answer')->insert([
        	[
        	'id'	      => 1,
            'a_text'      => '2',
            'is_correct'  => 1,
            'question_id' => 1
        ],
        [
        	'id'	      => 2,
            'a_text'      => '16',
            'is_correct'  => 1,
            'question_id' => 2
        ],
        [
        	'id'	      => 3,
            'a_text'      => '8',
            'is_correct'  => 0,
            'question_id' => 2
        ],
        [
        	'id'	      => 4,
            'a_text'      => '32',
            'is_correct'  => 0,
            'question_id' => 2
        ]

        ]);
    }
}
