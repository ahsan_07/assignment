<?php

use Illuminate\Database\Seeder;

class QuestionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('question')->insert([
        	[
        	'id'	            => 1,
            'q_text'            => 'What is square root of 4 ?',
            'q_type'            => 1,
            'questionare_id'    => 1
        ],
        [
        	'id'	            => 2,
            'q_text'            => 'What is square of 4 ?',
            'q_type'            => 2,
            'questionare_id'    => 1
        ]

        ]);
    }
}
