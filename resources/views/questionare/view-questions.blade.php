@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading"><h3>Add Questions</h3></div>

                <div class="panel-body">
                    <div class="alert alert-success hidden">
                      <strong>Success!</strong> Questions Updated
                    </div>
                    <input type="hidden" id="questionare-id" value="{{$questionare_id}}">
                    @php $i = 1;@endphp
                    @foreach($questions_with_answers as $question)
                        @php
                        $answers = $question->answers;
                        @endphp
                        <div class="single-question">
                            <div class="form-group question-type-div">
                                <label>Question Type</label>
                                <select class="question-type form-control" style="width:270px;">
                                        <option value="">Select Type</option>
                                    @if($question->q_type==1)
                                        <option value="1" selected>Short Answer</option>
                                        <option value="2">Multiple Choice (Single Option)</option>
                                        <option value="3">Multiple Choice (Multiple Options)</option>
                                    @elseif($question->q_type==2)
                                        <option value="1">Short Answer</option>
                                        <option value="2" Selected>Multiple Choice (Single Option)</option>
                                        <option value="3">Multiple Choice (Multiple Options)</option>
                                    @else
                                        <option value="1">Short Answer</option>
                                        <option value="2">Multiple Choice (Single Option)</option>
                                        <option value="3" Selected>Multiple Choice (Multiple Options)</option>
                                    @endif
                                </select>
                            </div>
                            
                            <div class="row form-group question-div">
                                <div class="col-sm-12 col-md-12">
                                    <div class="col-md-8 col-sm-8">
                                    <label>Question</label>
                                        <input type="text" name="question-text" placeholder="Enter Question" class="question-text form-control" value="{{$question->q_text}}">
                                    </div>
                                    <div class="col-sm-4 col-md-4">
                                        <br>
                                        <a href="#" class="dlt-question">Delete Question</a>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group answer-div">
                                @if($question->q_type==1)
                                    @foreach($answers as $answer)
                                        <div class="ans-div">
                                          <label>Answer</label>
                                          <input type="text" name="txt-ans" class="txt-ans form-control" style="width:470px" value="{{$answer->a_text}}">  
                                        </div>
                                        
                                    @endforeach
                                @elseif($question->q_type==2)
                                    <div class="ans-div">
                                        @foreach($answers as $answer)
                                            <div class="row my-ans-div">
                                                <div class="col-sm-12 col-md-12">
                                                    <div class="col-sm-5 col-md-5">
                                                        <label>Choice</label>
                                                        <input type="text" name="" class="form-control txt-ans" value="{{$answer->a_text}}">
                                                    </div>
                                                    <div class="col-md-2 col-sm-2">
                                                        <label style="margin-top:10px">Correct ?</label>
                                                    </div>
                                                    <div class="col-sm-2 col-md-2">
                                                         
                                                        @if($answer->is_correct==1)
                                                            <input type="radio" name="is-correct-{{$i}}" class="form-control is-correct" checked>
                                                        @else
                                                            <input type="radio" name="is-correct-{{$i}}" class="form-control is-correct">
                                                        @endif

                                                    </div>
                                                    <div class="col-sm-3 col-md-3">
                                                        <br>
                                                        <a href="#" class="remove-choice-btn">Remove Choice</a>
                                                    </div>
                                                    
                                                </div>
                                            </div>
                                        @endforeach
                                        <div class="radio-appender"></div>
                                        <a href="#" class="add-choice-radio-btn">Add Choice</a>
                                    </div>
                                @else
                                    <div class="ans-div">
                                        @foreach($answers as $answer)
                                            <div class="row my-ans-div">
                                                <div class="col-sm-12 col-md-12">
                                                    <div class="col-sm-5 col-md-5">
                                                        <label>Choice</label>
                                                        <input type="text" name="" class="form-control txt-ans" value="{{$answer->a_text}}">
                                                    </div>
                                                    <div class="col-md-2 col-sm-2">
                                                        <label style="margin-top:10px">Correct ?</label>
                                                    </div>
                                                    <div class="col-sm-2 col-md-2">
                                                         
                                                        @if($answer->is_correct==1)
                                                            <input type="checkbox" name="is-correct-{{$i}}" class="form-control is-correct" checked>
                                                        @else
                                                            <input type="checkbox" name="is-correct-{{$i}}" class="form-control is-correct">
                                                        @endif

                                                    </div>
                                                    <div class="col-sm-3 col-md-3">
                                                        <br>
                                                        <a href="#" class="remove-choice-btn">Remove Choice</a>
                                                    </div>
                                                    
                                                </div>
                                            </div>
                                        @endforeach
                                        <div class="check-appender"></div>
                                        <a href="#" class="add-choice-check-btn">Add Choice</a>
                                    </div>
                                @endif
                                
                            </div>
                            <hr>
                        </div>
                        @php $i++; @endphp
                    @endforeach
                    <div class="question-appender"></div>
                    <a href="#" class="add-question">Add new Question</a><br><br>
                    <button class="btn btn-primary update-all">Update</button>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
