@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading"><h4>Edit Questionair</h4></div>

                <div class="panel-body">
                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form id="edit-questionare-form" method="POST">
                        {{ csrf_field() }}
                      <div class="form-group">
                        <label>Questionair Name</label>
                        <input type="text" class="form-control" name="name" id="name" placeholder="Enter Questionair Name" value="{{$questionare->name}}" required>
                      </div>
                      <label>Duration</label>
                      <div class="form-group row">
                        <div class="col-md-12">
                            <div class="col-md-8">
                                <input type="text" class="form-control" name="duration" id="duration" placeholder="Enter Duration" value="{{$questionare->duration}}" required>
                            </div>
                            <div class="col-md-4">
                                <select class="form-control" name="unit" id="unit" required>
                                    @if($questionare->unit=='hr')
                                        <option value="hr" selected>Hours</option>
                                        <option value="min">Minutes</option>
                                    @else
                                        <option value="hr">Hours</option>
                                        <option value="min" selected>Minutes</option>
                                    @endif

                                </select>
                            </div>
                        </div>
                      </div>
                      <div class="form-group">
                        Can Resume ?
                        @if($questionare->is_resumable==true)
                            <label class="radio-inline">
                                <input type="radio" name="is_resumable" value="yes" required checked> Yes
                            </label>
                            <label class="radio-inline">
                              <input type="radio" name="is_resumable" value="no" required> No
                            </label>
                        @else
                            <label class="radio-inline">
                                <input type="radio" name="is_resumable" value="yes" required checked> Yes
                            </label>
                            <label class="radio-inline">
                              <input type="radio" name="is_resumable" value="no" required checked> No
                            </label>
                        @endif
                      </div>
                      <button type="submit" class="btn btn-default">Update</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
