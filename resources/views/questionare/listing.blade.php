@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading"><h3>Questionairs</h3></div>

                <div class="panel-body">

                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                    <a href="{{url('/create-questionair')}}">Create New</a><br><br>
                    <table id="listing-table" class="table table-hover table-bordered">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Name</th>
                                <th>Number of Questions</th>
                                <th>Duration</th>
                                <th>Resumeable</th>
                                <th>Published</th>
                                <th>Edit Action</th>
                                <th>Delete Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $i=1;?>
                            @foreach($questionares as $questionare)
                                <tr>
                                    <td>{{$i++}}</td>
                                    <td>{{$questionare->name}}</td>
                                    <td>{{$questionare->questions_count}} | <a href="{{url('/add-questions/'.$questionare->id)}}">Add</a></td>
                                    <td>{{$questionare->duration}}{{$questionare->unit}}</td>
                                    <td>
                                        @if($questionare->is_resumable==true)
                                        Yes
                                        @else
                                        No
                                        @endif
                                    </td>
                                    <td>
                                        @if($questionare->is_published==true)
                                        Yes
                                        @else
                                        No
                                        @endif
                                    </td>
                                    <td><a href="{{url('/edit-questionare/'.$questionare->id)}}">Edit</a></td>
                                    <td>
                                    <form method="POST" id="delete-questionare-form" action="{{url('/delete-questionare/'.$questionare->id)}}">
                                        {{ csrf_field() }}
                                        {{ method_field('DELETE') }}
                                        <button type="submit" class="dlt-btn btn btn-link">Delete</button>
                                    </form>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection