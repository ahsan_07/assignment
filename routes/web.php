<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes();

Route::get('/home', 'HomeController@index');
Route::get('/questionares', 'QuestionareController@index');
Route::get('/create-questionair', 'QuestionareController@create');
Route::post('/create-questionair', 'QuestionareController@store');
Route::get('/edit-questionare/{questionare_id}', 'QuestionareController@edit');
Route::post('/edit-questionare/{questionare_id}', 'QuestionareController@update');
Route::delete('/delete-questionare/{questionare_id}', 'QuestionareController@destroy');
Route::get('/add-questions/{questionare_id}', 'QuestionareController@viewQuestions');
Route::post('/update-questions-answers', 'QuestionareController@updateQuestionsAnswers');
