<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Questionare;
use App\Models\Question;
use App\Models\Answer;
use Auth;

class QuestionareController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $questionares = Questionare::withCount('questions')->get();
        return view('questionare.listing')->with('questionares',$questionares);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('questionare.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name'        => 'required|string',
            'duration'    => 'required|numeric',
        ]);

        if($request->is_resumable=='yes') {
            $resumable = true;
        }
        else {
            $resumable = false;
        }
        $user_id =  Auth::id();

        $questionare = new Questionare;
        $questionare->name = $request->name;
        $questionare->duration = $request->duration;
        $questionare->unit = $request->unit;
        $questionare->is_resumable = $resumable;
        $questionare->is_published = true;
        $questionare->publisher_id = $user_id;
        $questionare->save();
        return redirect('/questionares')->with('status', 'New Questionare Created'); 
    }

    

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $questionare = Questionare::findorfail($id);
        return view('questionare.edit')->with('questionare',$questionare);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        $this->validate($request, [
            'name'        => 'required|string',
            'duration'    => 'required|numeric',
        ]);

        if($request->is_resumable=='yes') {
            $resumable = true;
        }
        else {
            $resumable = false;
        }

        $questionare = Questionare::findorfail($id);
        $questionare->name = $request->name;
        $questionare->duration = $request->duration;
        $questionare->unit = $request->unit;
        $questionare->is_resumable = $resumable;
        $questionare->save();
        return redirect('/questionares')->with('status', 'Questionare updated!'); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $questionare = Questionare::findorfail($id);
        $questionare->delete();
        return redirect('/questionares')->with('status', 'Questionare Deleted!'); 
    }


    public function viewQuestions($id) {
        $questions_with_answers = Question::with('answers')->where('questionare_id', '=', $id)->get();
        return view('questionare.view-questions')->with('questions_with_answers',$questions_with_answers)->with('questionare_id', $id);
    }

    public function updateQuestionsAnswers(Request $request) {
        $input_arr = $request->input_arr;
        $questionare = Questionare::findorfail($request->q_id);
        $questionare->questions()->delete();

        foreach ($input_arr as $q_arr) {
            $question = new Question();
            $question->q_text = $q_arr['q_txt'];
            $question->q_type = $q_arr['q_type'];
            $questionare->questions()->save($question);

            foreach ($q_arr['answers'] as $ans_obj) {
                $answer = new Answer();
                $answer->a_text = $ans_obj['answer_txt'];
                $answer->is_correct = $ans_obj['is_correct'];
                $question->answers()->save($answer);
            }
        }
        echo true;
    }
}
