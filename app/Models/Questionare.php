<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Questionare extends Model
{
    protected $table = 'questionare';

    public function questions()
    {
        return $this->hasMany('App\Models\Question');
    }
}
