<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    protected $table = 'question';

    public function questionare()
    {
        return $this->belongsTo('App\Models\Questionare');
    }


    public function answers()
    {
        return $this->hasMany('App\Models\Answer');
    }
}
