$( document ).ready(function() {

	$.ajaxSetup({
	        headers: {
	            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	        }
	});

	$('.dlt-btn').click(function(event) {
		event.preventDefault();
		var go = confirm("Are You Sure ?");
		if(go==true) {
			$('#delete-questionare-form').submit();
		}
		else {
			return;
		}
	});


	$('.add-question').click(function(event) {
		event.preventDefault();
		var appender = '<div class="single-question"><div class="form-group question-type-div"><label>Question Type</label><select class="question-type form-control" style="width:270px;"><option value="">Select Type</option><option value="1">Short Answer</option><option value="2">Multiple Choice (Single Option)</option><option value="3">Multiple Choice (Multiple Options)</option></select></div><div class="row form-group question-div"><div class="col-sm-12 col-md-12"><div class="col-md-8 col-sm-8"><label>Question</label><input type="text" name="question-text" placeholder="Enter Question" class="question-text form-control"></div><div class="col-sm-4 col-md-4"><br><a href="#" class="dlt-question">Delete Question</a></div></div></div><div class="form-group answer-div"></div><hr></div>';
		$('.question-appender').append(appender);
	});

});


$( document).on( "click", ".dlt-question", function(e) {
	e.preventDefault();
  	$(this).parents().eq(3).remove();
});

$( document).on( "click", ".remove-choice-btn", function(e) {
	e.preventDefault();
  	$(this).parents().eq(2).remove();
});


$( document).on( "click", ".add-choice-radio-btn", function(e) {
	e.preventDefault();
  	var parent = $(this).parent();
  	var rname = parent.find('.is-correct').eq(0).attr('name');
  	if(rname==null){

  		rname = 'is-correct-'+Math.floor((Math.random() * 1000) + 1); ;
  	}
  	var radio_structure = '<div class="row my-ans-div"><div class="col-sm-12 col-md-12"><div class="col-sm-5 col-md-5"><label>Choice</label><input type="text" name="" class="form-control txt-ans"></div><div class="col-md-2 col-sm-2"><label style="margin-top:10px">Correct ?</label></div><div class="col-sm-2 col-md-2"><input type="radio" name="'+rname+'" class="form-control is-correct"></div><div class="col-sm-3 col-md-3"><br><a href="#" class="remove-choice-btn">Remove Choice</a></div></div></div>';
  	parent.find('.radio-appender').append(radio_structure);
});


$( document).on( "click", ".add-choice-check-btn", function(e) {
	e.preventDefault();
  	var parent = $(this).parent();
  	var cname = parent.find('.is-correct').eq(0).attr('name');
  	if(cname==null){

  		cname = 'is-correct-'+Math.floor((Math.random() * 1000) + 1); ;
  	}
  	var check_structure = '<div class="row my-ans-div"><div class="col-sm-12 col-md-12"><div class="col-sm-5 col-md-5"><label>Choice</label><input type="text" name="" class="form-control txt-ans"></div><div class="col-md-2 col-sm-2"><label style="margin-top:10px">Correct ?</label></div><div class="col-sm-2 col-md-2"><input type="checkbox" name="'+cname+'" class="form-control is-correct"></div><div class="col-sm-3 col-md-3"><br><a href="#" class="remove-choice-btn">Remove Choice</a></div></div></div>';
  	parent.find('.check-appender').append(check_structure);
});


$( document).on( "change", ".question-type", function(e) {
	e.preventDefault();
	var value = $(this).val();
	if(value=="") {
		return;
	}
	var answer_div = $(this).parents().eq(1).find('.answer-div');
	answer_div.empty();
	var appender = "";
	ename = 'is-correct-'+Math.floor((Math.random() * 1000) + 1); ;
	if(value=="1") {
		appender = '<div class="ans-div"><label>Answer</label><input type="text" name="txt-ans" class="txt-ans form-control" style="width:470px"></div>';
	}
	else if(value=="2") {
		appender = '<div class="ans-div"><div class="row my-ans-div"><div class="col-sm-12 col-md-12"><div class="col-sm-5 col-md-5"><label>Choice</label><input type="text" name="" class="form-control txt-ans"></div><div class="col-md-2 col-sm-2"><label style="margin-top:10px">Correct ?</label></div><div class="col-sm-2 col-md-2"><input type="radio" name="'+ename+'" class="form-control is-correct"></div><div class="col-sm-3 col-md-3"><br><a href="#" class="remove-choice-btn">Remove Choice</a></div></div></div><div class="radio-appender"></div><a href="#" class="add-choice-radio-btn">Add Choice</a></div>';
	}
	else if(value=="3") {
		appender = '<div class="ans-div"><div class="row my-ans-div"><div class="col-sm-12 col-md-12"><div class="col-sm-5 col-md-5"><label>Choice</label><input type="text" name="" class="form-control txt-ans"></div><div class="col-md-2 col-sm-2"><label style="margin-top:10px">Correct ?</label></div><div class="col-sm-2 col-md-2"><input type="checkbox" name="'+ename+'" class="form-control is-correct"></div><div class="col-sm-3 col-md-3"><br><a href="#" class="remove-choice-btn">Remove Choice</a></div></div></div><div class="check-appender"></div><a href="#" class="add-choice-check-btn">Add Choice</a></div>';
	}
	answer_div.append(appender);
});


$( document).on( "click", ".update-all", function(e) {
	e.preventDefault();
	var elements = $('.single-question');
	var input_arr = [];
	var q_id = $('#questionare-id').val();

	$.each(elements, function( index, value ) {
		var q_type = $(value).find('.question-type').val();
		var q_txt = $(value).find('.question-text').val();
		var ans_arr = [];

		if(q_type=="1") {
			var answers = $(value).find('.txt-ans').val();
			var ans_obj = {answer_txt:answers, is_correct:1};
			ans_arr.push(ans_obj);

		}
		else if(q_type=="2" || q_type=="3") {
			var answers = $(value).find('.my-ans-div');
			$.each(answers, function( aindex, avalue ) {
				var anstxt = $(avalue).find('.txt-ans').val();
				var is_corr_elem = $(avalue).find('.is-correct');
				if($(is_corr_elem).is(':checked')) {
					var is_corr = 1;
				}
				else {
					var is_corr = 0;
				}
				var ans_obj = {answer_txt:anstxt, is_correct:is_corr};
				ans_arr.push(ans_obj);

			});
		}
	 	
	 	var myobj = {q_type:q_type, q_txt: q_txt, answers: ans_arr};
	 	input_arr.push(myobj);
	});
	//console.log(input_arr);

	$.ajax({
		type      :  "POST",
		url       :  "/update-questions-answers",
		data      :  {'input_arr': input_arr, 'q_id':q_id},
		beforeSend: function()
		{
		},
		success : function(response)
		{
			$('.alert-success').removeClass('hidden');
		},
		error :  function()
		{
			console.log('something went wrong');
		},
		complete: function()
		{	
		}
	});

});